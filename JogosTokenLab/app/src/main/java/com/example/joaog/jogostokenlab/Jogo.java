package com.example.joaog.jogostokenlab;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by joaog on 20/11/2016.
 */
//Classe para armazenar os jogos.
public class Jogo implements Serializable {
    public String name;
    public String url_image;
    public String release_date;
    public String url_trailer;
    public ArrayList platforms;

    @Override
    public String toString() {
        return "Jogo{" + " nome=' " + name + '\'' + '}';
    }
}


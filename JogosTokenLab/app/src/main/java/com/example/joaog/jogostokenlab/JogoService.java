package com.example.joaog.jogostokenlab;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import livroandroid.lib.utils.HttpHelper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by joaog on 20/11/2016.
 */

public class JogoService {
    private static final boolean LOG_ON = true;
    private static final String TAG = "JogoService";
    private static final String url = "https://dl.dropboxusercontent.com/u/34048947/games";

    private static List<Jogo> parserJSON (String json) throws IOException {
        List<Jogo> jogos = new ArrayList<Jogo>();
        try {
            //cria um objeto e um array JSON para pegar os jogos do Dropbox
            JSONObject root = new JSONObject(json);
            JSONArray jsonJogos = root.getJSONArray("games");

            //insere cada jogo na lista
            for (int i=0; i<jsonJogos.length();i++){
                JSONObject jsonJogo = jsonJogos.getJSONObject(i);
                Jogo j = new Jogo();

                //ArrayList para salvar as plataformas
                ArrayList<String> platforms = new ArrayList<String>();
                JSONArray temp = new JSONArray();

                //Obtém as informações para salvar o jogo na lista.
                j.name = jsonJogo.optString("name");
                j.url_image = jsonJogo.optString("image");
                j.release_date = jsonJogo.optString("release_date");
                j.url_trailer = jsonJogo.optString("trailer");
                temp = jsonJogo.optJSONArray("platforms");

                //Converte o JSONArray em Array
                if (temp != null) {
                    for (int g=0;g<temp.length();g++){
                        platforms.add(temp.get(g).toString());
                    }
                }
                j.platforms = platforms;
                if (LOG_ON){
                    Log.d(TAG, "Jogo " + j.name);
                    Log.d(TAG, "Plataformas " + j.platforms);
                }
                jogos.add(j);
            }
            if (LOG_ON){
                Log.d (TAG, jogos.size() + " encontrados");
            }
        }
        catch (JSONException e){
            throw new IOException(e.getMessage(), e);
        }
        return jogos;
    }

    public static List<Jogo> getJogos () throws IOException{

        //Instancia um objeto HttpHelper.
        HttpHelper http = new HttpHelper();

        //Chama o método doGet para obter as informações.
        String json = http.doGet(url);
        List <Jogo> jogo = parserJSON(json);
        return jogo;
    }

}




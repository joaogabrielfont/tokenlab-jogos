package com.example.joaog.jogostokenlab;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.ImageView;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.jaedongchicken.ytplayer.model.YTParams;
import com.squareup.picasso.Picasso;
import com.jaedongchicken.ytplayer.YoutubePlayerView;
import java.util.List;


public class GameActivity extends AppCompatActivity {
    private String name;
    private String image;
    private String release;
    private String trailer;
    private String ytId;
    private List<String> platforms;

    //Função que usa expressão regular para extrair a ID do vídeo do Youtube.
    public static String extractYTId(String ytUrl) {
        String vId = null;
        Pattern pattern = Pattern.compile(
                ".+=(.+)",
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(ytUrl);
        if (matcher.matches()){
            vId = matcher.group(1);
        }
        return vId;
    }

    //Função do botão voltar para a Home.
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        //Muda o titulo da ActionBar.
        setTitle(getString(R.string.detalhes));

        //Cria o botão voltar para a Home.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Pega o Bundle enviado pela MainActivity.
        Intent intent = getIntent();
        Bundle args = intent.getExtras();

        //Passa os parâmetros do Bundle para as variáveis locais.
        name = args.getString("name");
        image = args.getString ("image");
        release = args.getString("release");
        trailer = args.getString("trailer");
        platforms = args.getStringArrayList("platforms");
        ytId = extractYTId(trailer);

        //Instancia as TextViews;
        TextView nome = (TextView)findViewById(R.id.nameview);
        TextView lancamento = (TextView)findViewById(R.id.lancview);
        TextView plataformas = (TextView)findViewById(R.id.platform_view);

        //Coloca os dados nas TextViews;
        nome.setText(name);
        lancamento.setText(getString(R.string.data_lancamento) + "\n" + release);

        //Transforma a lista de plataformas em uma String;
        String lista_plataformas = new String();

        for (int i=0; i<platforms.size(); i++){
            if (i==0){
                lista_plataformas = platforms.get(i);
            }
            else {
                lista_plataformas = lista_plataformas + ", " + platforms.get(i);
            }
        }

        //Coloca a String plataformas no Textview;
        plataformas.setText(getString(R.string.plataformas) + "\n" + lista_plataformas);

        //Coloca a imagem na ImageView
        ImageView imagem = (ImageView) findViewById(R.id.imageView);
        Picasso.with(this).load(image).into(imagem);

        //Coloca o vídeo no YoutubePlayerView
        YoutubePlayerView youtubePlayerView = (YoutubePlayerView) findViewById(R.id.youtubePlayerView);
        YTParams params = new YTParams();
        youtubePlayerView.setAutoPlayerHeight(this);

        //Inicializa o YoutubePlayerView
        youtubePlayerView.initialize(ytId, new YoutubePlayerView.YouTubeListener() {

            @Override
            public void onReady() {

            }

            @Override
            public void onStateChange(YoutubePlayerView.STATE state) {

            }

            @Override
            public void onPlaybackQualityChange(String arg) {
            }

            @Override
            public void onPlaybackRateChange(String arg) {
            }

            @Override
            public void onError(String error) {
            }

            @Override
            public void onApiChange(String arg) {
            }

            @Override
            public void onCurrentSecond(double second) {
                // currentTime callback
            }

            @Override
            public void onDuration(double duration) {
                // total duration
            }

            @Override
            public void logs(String log) {
                // javascript debug log. you don't need to use it.
            }
        });
    }
}

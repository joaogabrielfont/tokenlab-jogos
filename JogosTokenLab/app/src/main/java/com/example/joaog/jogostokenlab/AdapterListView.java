package com.example.joaog.jogostokenlab;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.List;
import android.content.Context;
import android.widget.TextView;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;


/**
 * Created by joaog on 22/11/2016.
 */

public class AdapterListView extends BaseAdapter {
    private LayoutInflater mInflater;
    private List <Jogo> jogos;
    private Context context;

    //Função que cria a lista e preenche com os jogos
    public AdapterListView (Context context, List<Jogo> jogos){
        this.jogos = jogos;
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public int getCount(){
        return jogos.size();
    }

    public Jogo getItem(int position){
        return jogos.get(position);
    }

    public long getItemId(int position){
        return position;
    }

    public View getView(int position, View view, ViewGroup parent){
        //Pega o item de acordo com a posção.
        Jogo item = jogos.get(position);

        //infla o layout para podermos preencher os dados
        view = mInflater.inflate(R.layout.item_listview, null);

        //atraves do layout pego pelo LayoutInflater, pegamos cada id relacionado
        //ao item e definimos as informações.

        ((TextView) view.findViewById(R.id.text)).setText(item.name);
        ImageView imagem = (ImageView) view.findViewById(R.id.imagemview);
        Picasso.with(context).load(item.url_image).into(imagem);

        return view;
    }

}



package com.example.joaog.jogostokenlab;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ListView;
import android.view.View;
import android.content.Intent;
import java.io.IOException;
import java.util.List;



public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private List <Jogo> jogos;
    private ProgressDialog dialog;
    private ListView listView;
    private AdapterListView adapterListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Pega os jogos online
        this.taskJogos();

        //Instancia a lista
        listView = (ListView) findViewById(R.id.ListaJogos);
        listView.setOnItemClickListener(this);
        createListView();
    }

    //Chama o serviço de lista de jogos.
    private void taskJogos(){
        dialog = ProgressDialog.show(this, "Carregando os jogos", "Por favor, aguarde...", false, true);
        new Thread(){
            @Override
            public void run(){
                try{
                    //Chama o serviço.
                    jogos = JogoService.getJogos();

                    //Cria a lista na Thread principal.
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            createListView();
                        }
                    });
                }
                catch (IOException e){
                    Log.e("JogoService", e.getMessage(),e);
                }
                finally {
                    dialog.dismiss();

                }

            }

        }.start();
    }

    private void createListView() {
        //Cria o adapter
        if (jogos != null) {
            adapterListView = new AdapterListView(this, jogos);

            //Define o Adapter
            listView.setAdapter(adapterListView);

            //Cor quando a lista é selecionada para rolagem.
            listView.setCacheColorHint(Color.BLUE);

        }
    }

    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
    {
        //Pega o item que foi selecionado.
        Jogo item = adapterListView.getItem(arg2);

        //Cria o Intent para mudar de Activity.
        Intent intent = new Intent (this, GameActivity.class);

        //Cria o Bundle para enviar as informações para a nova Activity.
        Bundle params = new Bundle();

        //Coloca cada Item no Bundle.
        params.putString("name", item.name);
        params.putString("image", item.url_image);
        params.putString("release", item.release_date);
        params.putString ("trailer", item.url_trailer);
        params.putStringArrayList("platforms", item.platforms);

        //Coloca o Bundle no Intent.
        intent.putExtras(params);

        //Inicia a Activity.
        startActivity(intent);
    }
}

